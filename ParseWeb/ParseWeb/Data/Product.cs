﻿using System.ComponentModel.DataAnnotations;

namespace ParseWeb.Data
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
