﻿using System.ComponentModel.DataAnnotations;
namespace ParseWeb.Data
{
    public class Shop
    {
        [Key]
        public int ShopId { get; set; }
        public string Name { get; set; }
    }
}
