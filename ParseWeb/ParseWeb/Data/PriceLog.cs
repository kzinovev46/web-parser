﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ParseWeb.Data
{
    public class PriceLog
    {
        [Column(TypeName ="datetime")]
        public DateTime Timer { get; set; }
        [ForeignKey(nameof(Shop))]
        public int ShopId { get; set; }
        [ForeignKey(nameof(Product))]
        public int ProductId { get; set; }
        public double Price { get; set; }
    }
}
