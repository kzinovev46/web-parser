﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ParseWeb.Data;

namespace ParseWeb.Controllers
{
    public class PriceLogsController : Controller
    {
        private readonly ProductPricesContext _context;

        public PriceLogsController(ProductPricesContext context)
        {
            _context = context;
        }

        // GET: PriceLogs
        public async Task<IActionResult> Index()
        {
              return View(await _context.PriceLogs.ToListAsync());
        }

        // GET: PriceLogs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.PriceLogs == null)
            {
                return NotFound();
            }

            var priceLog = await _context.PriceLogs
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (priceLog == null)
            {
                return NotFound();
            }

            return View(priceLog);
        }

        // GET: PriceLogs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PriceLogs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Timer,ShopId,ProductId,Price")] PriceLog priceLog)
        {
            if (ModelState.IsValid)
            {
                _context.Add(priceLog);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(priceLog);
        }

        // GET: PriceLogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.PriceLogs == null)
            {
                return NotFound();
            }

            var priceLog = await _context.PriceLogs.FindAsync(id);
            if (priceLog == null)
            {
                return NotFound();
            }
            return View(priceLog);
        }

        // POST: PriceLogs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Timer,ShopId,ProductId,Price")] PriceLog priceLog)
        {
            if (id != priceLog.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(priceLog);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PriceLogExists(priceLog.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(priceLog);
        }

        // GET: PriceLogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.PriceLogs == null)
            {
                return NotFound();
            }

            var priceLog = await _context.PriceLogs
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (priceLog == null)
            {
                return NotFound();
            }

            return View(priceLog);
        }

        // POST: PriceLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.PriceLogs == null)
            {
                return Problem("Entity set 'ProductPricesContext.PriceLogs'  is null.");
            }
            var priceLog = await _context.PriceLogs.FindAsync(id);
            if (priceLog != null)
            {
                _context.PriceLogs.Remove(priceLog);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PriceLogExists(int id)
        {
          return _context.PriceLogs.Any(e => e.ProductId == id);
        }
    }
}
