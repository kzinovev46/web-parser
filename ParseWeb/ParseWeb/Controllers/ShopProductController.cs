﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ParseWeb.Data;

namespace ParseWeb.Controllers
{
 
        public class ShopProductController : Controller
    {
        private readonly ProductPricesContext _context;

        public ShopProductController(ProductPricesContext context)
        {
            _context = context;
        }
 
        /* public async Task<double> ParseGROHE(string link)
         {
             using var httpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };
             var response = await httpClient.GetAsync(link);
             var responseData = await response.Content.ReadAsStringAsync();
             if (response.StatusCode == System.Net.HttpStatusCode.OK)
             {
                 var startBlock = "\"bundle-totals__final-price\"";
                 var StartPos = responseData.IndexOf(startBlock);
                 var priceAllText = responseData.Substring(StartPos + startBlock.Length);
                 var priceText = priceAllText.Substring(0, priceAllText.IndexOf("*"));
                 var price = Convert.ToDouble(priceText);
                 return price;
             }

         } */

        [HttpPost]
        public async Task<IActionResult> Parse([Bind("ProductId,ShopId,Link,Price")] ShopProduct shopProduct)
        {
            /*if (shopProduct.ShopId==1)
            {
                shopProduct.Price = 1000;
                _context.Update(shopProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            } */
            var shopprods = _context.ShopProducts.ToList();
            var logs = new List<PriceLog>();
            foreach (ShopProduct sp in shopprods)
            {
                using var httpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };
               
                var response = await httpClient.GetAsync(sp.Link);
                var responseData = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (sp.ShopId == 1 || sp.ShopId == 2)
                    {
                        var startBlock = "";
                        var ind = "";
                        if (sp.ShopId == 1)
                        {
                            startBlock = "ecomm_totalvalue:\"";
                            ind = ".";
                        }
                        else if (sp.ShopId == 2)
                        {
                            startBlock = "itemprop=\"price\" content=\"";
                            ind = "\"";
                        }
                        var StartPos = responseData.IndexOf(startBlock);
                        Console.WriteLine(StartPos + "\n\n\n");
                        Console.WriteLine(StartPos + startBlock.Length + "\n\n\n");
                        var priceAllText = responseData.Substring(StartPos + startBlock.Length);
                        Console.WriteLine(priceAllText + "\n\n\n");
                        var priceText = priceAllText.Substring(0, priceAllText.IndexOf(ind));
                        Console.WriteLine(priceText + "\n\n\n");
                        var price = Convert.ToDouble(priceText);
                        sp.Price = price;
                        _context.Entry(sp).State = EntityState.Modified;
                    }
                    else
                    {
                        sp.Price = 0;
                    }
                }
                new PriceLog
                {
                    Timer = DateTime.Now,
                    ShopId= sp.ShopId,
                    ProductId= sp.ProductId,
                    Price=sp.Price
                };


            }
            //IEnumerable<ShopProduct> ShopProducts = _context.ShopProducts.Where(s => s.Shop.Name == "GROHE").AsEnumerable().Select(  s => { s.Price = ParseGROHE(s.Link); return s; });
            //foreach (ShopProduct sp in ShopProducts)
            //{
             //  _context.Entry(sp).State= EntityState.Modified;
            //}
            _context.SaveChanges();
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "ProductId", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "ShopId", shopProduct.ShopId);
            //return View(shopProduct);
            return RedirectToAction(nameof(Index));
            //return View();
        } 
        // GET: ShopProduct

        public async Task<IActionResult> Index()
        {
            var productPricesContext = _context.ShopProducts.Include(s => s.Product).Include(s => s.Shop);
            return View(await productPricesContext.ToListAsync());
        }

        // GET: ShopProduct/Details/5
        public async Task<IActionResult> Details(int? ShopId, int? ProductId)
        {
            if (ShopId == null || ProductId == null || _context.ShopProducts == null)
            {
                return NotFound();
            }

            var shopProduct = await _context.ShopProducts
                .Include(s => s.Product)
                .Include(s => s.Shop)
                .FirstOrDefaultAsync(m => m.ProductId == ProductId && m.ShopId == ShopId);
            if (shopProduct == null)
            {
                return NotFound();
            }

            return View(shopProduct);
        }

        // GET: ShopProduct/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name");
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name");
            return View();
        }

        // POST: ShopProduct/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,ShopId,Link,Price")] ShopProduct shopProduct)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shopProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name", shopProduct.ShopId);
            return View(shopProduct);
        }

        // GET: ShopProduct/Edit/?ProductId=0&ProductId=0
        public async Task<IActionResult> Edit(int? ShopId,int? ProductId)
        {
            if (ShopId == null || ProductId==null || _context.ShopProducts == null)
            {
                return NotFound();
            }

            var shopProduct = await _context.ShopProducts.FirstOrDefaultAsync(x => x.ShopId == ShopId && x.ProductId == ProductId);
            if (shopProduct == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name", shopProduct.ShopId);
            return View(shopProduct);
        }

        // POST: ShopProduct/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int ShopId,int ProductId,  [Bind("ProductId,ShopId,Link,Price")] ShopProduct shopProduct)
        {
            if (ProductId != shopProduct.ProductId || ShopId != shopProduct.ShopId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(shopProduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShopProductExists(shopProduct.ProductId, shopProduct.ShopId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name", shopProduct.ShopId);
            return View(shopProduct);
        }

        // GET: ShopProduct/Delete/5
        public async Task<IActionResult> Delete(int? ShopId, int? ProductId)
        {
            if (ProductId == null ||ShopId == null|| _context.ShopProducts == null)
            {
                return NotFound();
            }

            var shopProduct = await _context.ShopProducts
                .Include(s => s.Product)
                .Include(s => s.Shop)
                .FirstOrDefaultAsync(m => m.ProductId == ProductId && m.ShopId == ShopId);
            if (shopProduct == null)
            {
                return NotFound();
            }

            return View(shopProduct);
        }

        // POST: ShopProduct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int ShopId, int ProductId)
        {
            if (_context.ShopProducts == null)
            {
                return Problem("Entity set 'ProductPricesContext.ShopProducts'  is null.");
            }
            var shopProduct = await _context.ShopProducts.FindAsync(ProductId);
            if (shopProduct != null)
            {
                _context.ShopProducts.Remove(shopProduct);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ShopProductExists(int ShopId, int ProductId)
        {
          return _context.ShopProducts.Any(e => e.ProductId == ProductId);
        }
    }
}
